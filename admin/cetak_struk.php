<?php
include '../koneksi.php';
require('../owner/fpdf/fpdf.php');

$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->SetX(9.5); 
$pdf->SetFont('Arial','B',14);           
$pdf->MultiCell(10,0.5,'Deu Resto',0,'P');
$pdf->SetFont('Arial','B',10);
$pdf->SetX(9);
$pdf->MultiCell(10,0.5,'JL. Kampung Sawah',0,'P');
$pdf->SetX(6);
$pdf->MultiCell(10,0.5,'website : deuresto.sknc.site email : deuresto@gmail.com',0,'P');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$get_id=$_GET['id_order'];
$query1=mysqli_query($conn,"SELECT * from transaksi inner join user on transaksi.id_user=user.id_user where transaksi.id_order='$get_id'");
$lihat_kasir=mysqli_fetch_array($query1);
$pdf->Cell(5,0.7,"Tanggal : ".date("D-d/m/Y"),0,0,'C');
$pdf->Cell(19,0.7,"Kasir : ". $lihat_kasir['nama_user'],0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, '', 0, 0, 'C');
$pdf->Cell(5, 0.8, '', 0, 0, 'C');
$pdf->Cell(4, 0.8, '', 0, 0, 'C');
$pdf->Cell(4, 0.8, '', 0, 0, 'C');
$pdf->Cell(4, 0.8, '', 0, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$get_id=$_GET['id_order'];
$query=mysqli_query($conn,"SELECT * from tborder inner join detail_order on tborder.id_order=detail_order.id_order inner join masakan on detail_order.id_masakan=masakan.id_masakan where tborder.id_order='$get_id'");
while($lihat=mysqli_fetch_array($query)){
	$jumlah=$lihat['jumlah']*$lihat['harga'];
	$harga=$lihat['harga'];
	$hasil="Rp".number_format($harga,2,',','.');
	$hasil_kali="Rp".number_format($jumlah,2,',','.');
	$pdf->Cell(1, 0.8, $no , 0, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_masakan'],0, 0, 'C');
	$pdf->Cell(4, 0.8, $hasil, 0, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['jumlah'],0, 0, 'C');
	$pdf->Cell(4, 0.8, $hasil_kali,0, 1, 'C');
	$no++;
}
$query1=mysqli_query($conn,"SELECT * from transaksi where id_order='$get_id'");
$lihat_id=mysqli_fetch_array($query1);
	$harga=$lihat_id['total_bayar'];
	 $hasil="Rp.".number_format($harga,2,',','.');
	 $harga1=$lihat_id['uang'];
	 $hasil1="Rp.".number_format($harga1,2,',','.');
	 $harga2=$lihat_id['kembalian'];
	 $hasil2="Rp.".number_format($harga2,2,',','.');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->SetX(8);
$pdf->Cell(16,0.7,"Total Bayar    :          ".$hasil,0,0,'C');
$pdf->ln(1);
$pdf->SetX(8);
$pdf->Cell(16.5,0.7,"Tunai        :          ".$hasil1,0,0,'C');
$pdf->ln(1);
$pdf->SetX(8);
$pdf->Cell(15.5,0.7,"    Kembali      :       ".$hasil2,0,0,'C');

$pdf->SetX(9);
$pdf->MultiCell(10,3,'Terima Kasih',0,'P');

$pdf->Output("struk.pdf","I");
?>

