<?php
include '../koneksi.php';
require('pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('pdf/logo.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'DeUResto',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 089658379697',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jl. Kampung Sawah RT 02 RW 02 No. 39',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.deuresto.com : deuresto@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Makanan",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'No', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Nama', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Harga', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Kategori', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'Status Makanan', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($conn, "SELECT*FROM masakan inner join kategori on masakan.id_kategori=kategori.id_kategori");
while($lihat=mysqli_fetch_array($query)){
	$harga=$lihat['harga'];
	$hasil="Rp".number_format($harga,2,',',',');
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_masakan'],1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['harga'], 1, 0,'C');
	$pdf->Cell(6, 0.8, $lihat['nama_kategori'],1, 0, 'C');
	if($lihat['status_masakan']='Y'){
		$pdf->Cell(4.5, 0.8, 'Tersedia', 1, 1, 'C');
	}else{
		$pdf->Cell(4.5, 0.8, 'Tidak Tersedia', 1, 1, 'C');
	}
	$no++;
}

$pdf->Output("laporan_data_makanan.pdf","I");

?>

