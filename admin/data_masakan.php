 <?php
 include 'header_admin.php';
 include'database.php';
$db = new database();
 ?>
 <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">             
              <div class="box">
                <div class="table-responsive">
                <div class="box-header">
                  <h3 class="box-title">Data Menu Makanan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="agile3-grids">
            <p align="left"><a href="#tambahmasakan" data-toggle="modal" class="btn btn-primary">Tambah Data</a>
            <a href="lap_makanan.php" class="btn btn-success" target="blank">Cetak PDF</a></p>
          </div>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Masakan</th>
                        <th>Harga</th>
                        <th>Kategori</th>
                        <th>Gambar</th>
                        <th>Status Masakan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
$no = 1;
foreach($db->tampil_data_masakan() as $x){
?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $x['nama_masakan']; ?></td>
                        <td><?php echo $x['harga']; ?></td>
                        <td><?php echo $x['nama_kategori']; ?></td>
                        <td><img src="../images/<?php echo $x['gambar']; ?>" height='100'></td>
                        <td>
                          <?php
                          if($x['status_masakan'] == 'Y')
                          {
                          ?>
                          <a href="approve_masak.php?table=masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=not-verifed" class="btn btn-info btn-md">
                          Tersedia
                          </a>
                          <?php
                          }else{
                          ?>
                          <a href="approve_masak.php?table=masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                          Habis
                          </a>
                          <?php
                          }
                          ?>
                        </td>
                        <td>
                        <a href="" data-toggle="modal" data-target="#myModal<?php echo $x['id_masakan'];?>" class="btn btn-primary">Edit</a>      
                        </td>
                      </tr>
                      <div class="modal" id="myModal<?php echo $x['id_masakan'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Menu</h4>
                  </div>
<?php
include '../koneksi.php';
$id = $x['id_masakan']; 
$query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
$r = mysqli_fetch_array($query_edit);
?>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="update_makanan.php?id_masakan=<?php echo $r['id_masakan'];?>" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="form-group">
                              <label for="nama_masakan">Nama Masakan :</label>
                              <input type="hidden" name="id_masakan" value="<?php echo $r['id_masakan'];?>">
                              <input type="text" class="form-control" name="nama_masakan" id="nama_masakan" value="<?php echo $r['nama_masakan'];?>" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                              <label for="harga">Harga :</label>
                              <input type="integer" class="form-control" name="harga" id="harga" value="<?php echo $r['harga'];?>" placeholder="Masukan Harga">
                            </div>
                            <div class="form-group">
                                 <label for="id_kategori">Kategori :</label>
                                 <div class="col-md-12">
                                    <select name="id_kategori" class="form-control col-md-12">
                                    <?php     
                                    include"../koneksi.php";
                                      $select=mysqli_query($conn, "SELECT * FROM kategori");
                                      while($show=mysqli_fetch_array($select)){
                                    ?>
                                      <option value="<?=$show['id_kategori'];?>" <?=$r['id_kategori']==$show['id_kategori']?'selected':null?>><?=$show['nama_kategori'];?></option>
                                      <?php } ?>
                                  </select>
                                </div>
                                </div>
                            <div class="form-group">
                              <label for="Gambar">Masukan Gambar :</label>
                              <input type="file" name="gambar" value="gambar/<?php echo $r['gambar'];?>">
                            </div>
                                
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                      
<?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <div class="modal" id="tambahmasakan">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Menu Masakan</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_tambah_masakan.php" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Nama Masakan :</label>
                              <input type="text" class="form-control" name="nama_masakan" id="exampleInputPassword1" placeholder="Masukan Nama Masakan" required="">
                            </div>
                             
                            <div class="form-group">
                              <label for="exampleInputPassword1">Harga :</label>
                              <input type="integer" class="form-control" name="harga" id="exampleInputPassword1" placeholder="Masukan Harga" required="">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputFile">Masukan Gambar :</label>
                              <input type="file" accept="image/*" name="gambar" required="">                           
                            </div>
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Simpan</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php
include 'footer_admin.php';
?>