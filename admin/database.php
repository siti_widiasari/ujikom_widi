<?php 

class database{

	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "kasir";
	public $mysqli;

	function __construct(){
		$this->mysqli = new mysqli($this->host, $this->uname, $this->pass ,$this->db);
	}

	function tampil_data_masakan(){
		$data =$this->mysqli->query("SELECT * FROM masakan INNER JOIN kategori ON masakan.id_kategori = kategori.id_kategori order by id_masakan DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_data_user(){
		$data =$this->mysqli->query("SELECT * from user INNER JOIN level ON user.id_level = level.id_level");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_belum_bayar(){
		$data =$this->mysqli->query("SELECT tborder.id_order, tborder.no_meja, tborder.tanggal, user.nama_user,tborder.keterangan,tborder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from tborder INNER JOIN user ON tborder.id_user = user.id_user inner join transaksi on tborder.id_order=transaksi.id_order where transaksi.keterangan_transaksi='N' and tborder.status_order='Y' order by id_transaksi DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_data_order(){
		$data =$this->mysqli->query("SELECT tborder.id_order, tborder.no_meja, tborder.tanggal, user.nama_user, tborder.keterangan,tborder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from tborder INNER JOIN user ON tborder.id_user = user.id_user inner join transaksi on tborder.id_order=transaksi.id_order where keterangan_transaksi='N' order by id_order DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_detail_order(){
		$data =$this->mysqli->query("SELECT *,detail_order.jumlah as jml from detail_order INNER JOIN masakan ON masakan.id_masakan= detail_order.id_masakan where detail_order.id_order='$_GET[id_order]'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_data_pesanan(){
		$data =$this->mysqli->query("SELECT * from masakan where status_masakan='Y'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_pesanan(){
		$data =$this->mysqli->query("SELECT tborder.id_order, tborder.no_meja, tborder.tanggal, user.nama_user,tborder.keterangan,tborder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from tborder INNER JOIN user ON tborder.id_user = user.id_user inner join transaksi on tborder.id_order=transaksi.id_order where keterangan_transaksi='Y' order by id_order DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function detail_tampil_pesanan(){
		$data =$this->mysqli->query("SELECT *,sum(detail_order.jumlah) as jml from detail_order INNER JOIN masakan ON masakan.id_masakan= detail_order.id_masakan where detail_order.id_order='$_GET[id_order]' group by detail_order.id_masakan");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function daftar_meja(){
		$data =$this->mysqli->query("SELECT * from meja  ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function edit_masakan($id_masakan){
		$data = $this->mysqli->query("SELECT * from masakan where id_masakan='$id_masakan'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}function update_user($id_user,$username,$password_aman,$email,$nama_user,$id_level){
		$data = $this->mysqli->query("update user set username='$username', password='$password_aman', email='$email', nama_user='$nama_user', id_level='$id_level' where id_user='$id_user'");
	}

	function update_masakan($id_masakan,$nama_masakan,$harga,$gambar,$status_masakan){
		$data = $this->mysqli->query("update masakan set nama_masakan='$nama_masakan', harga='$harga', gambar='$gambar', status_masakan='$status_masakan' where id_masakan='$id_masakan'");
	}
	function input_user($username,$password,$nama_user,$email,$id_level,$status){
		$data =$this->mysqli->query("INSERT into user values('','$username','$password','$nama_user','$email','$id_level','Y')");
	}
	function input_meja($no_meja,$status_meja){
		$data =$this->mysqli->query("INSERT into meja values('','$no_meja','$status_meja')");
	}
} 

?>