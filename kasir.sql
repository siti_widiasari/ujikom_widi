-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 09:42 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id_detail_order` int(10) NOT NULL,
  `id_order` int(10) NOT NULL,
  `id_masakan` int(10) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_detail_order` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `jumlah`, `keterangan`, `status_detail_order`) VALUES
(17, 13, 7, 1, 'yuk', 'Y'),
(18, 13, 8, 1, 'b', 'Y'),
(19, 14, 8, 1, 'pedesss', 'Y'),
(20, 15, 11, 1, 'o', 'Y'),
(21, 16, 9, 1, 'nn', 'Y'),
(22, 17, 11, 1, 'r', 'Y'),
(23, 18, 16, 1, 'b', 'Y'),
(24, 19, 7, 1, 'u', 'Y'),
(25, 19, 8, 1, 'b', 'Y'),
(26, 20, 9, 1, 'n', 'Y'),
(27, 20, 21, 1, 'b', 'Y'),
(28, 21, 16, 1, 'bii', 'Y'),
(29, 21, 20, 1, 'oo', 'Y'),
(30, 22, 11, 1, 'pedess', 'Y'),
(31, 23, 16, 1, 'pake cabe', 'Y'),
(32, 24, 11, 1, 'v', 'Y'),
(33, 25, 8, 1, 'b', 'Y'),
(34, 25, 7, 1, 'h', 'Y'),
(35, 26, 8, 1, 'v', 'Y'),
(36, 27, 11, 1, 'b', 'Y'),
(37, 28, 16, 1, 'y', 'Y'),
(38, 29, 9, 1, 'c', 'N'),
(39, 30, 9, 1, 'jangan pake sayur\r\n', 'Y'),
(40, 31, 9, 1, 'tu', 'Y'),
(41, 32, 8, 1, 'o', 'Y'),
(42, 32, 8, 2, 'u', 'Y'),
(43, 32, 8, 1, 'm', 'Y'),
(44, 32, 9, 1, '', 'Y'),
(45, 33, 7, 1, 't', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(10) NOT NULL,
  `nama_kategori` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'makanan'),
(2, 'minuman');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(10) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'waiter'),
(3, 'kasir'),
(4, 'owner'),
(5, 'pelanggan');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE `masakan` (
  `id_masakan` int(10) NOT NULL,
  `id_kategori` int(10) NOT NULL,
  `nama_masakan` varchar(50) NOT NULL,
  `harga` varchar(30) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `status_masakan` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Habis, Y Tersedia'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `id_kategori`, `nama_masakan`, `harga`, `gambar`, `status_masakan`) VALUES
(6, 1, 'mie gorenggggyuk', '15000', 'download (1).jpg', 'N'),
(7, 1, 'seblak kuah', '7000', 's4.jpg', 'Y'),
(8, 1, 'lumpia basah', '7000', 's2.jpg', 'Y'),
(9, 1, 'mie ayam', '15000', 's6.jpg', 'Y'),
(10, 1, 'kue pisang', '50000', 'download (3).jpg', 'N'),
(11, 1, 'mie aceh', '25000', 's3.jpg', 'Y'),
(12, 1, 'seblak makaroni', '10000', 'images (5).jpg', 'N'),
(13, 1, 'seblak makaroni', '10000', 'images (2).jpg', 'N'),
(14, 1, 'bihun', '5000', 's5.jpg', 'Y'),
(15, 1, 'soto', '15000', 's3.jpg', 'Y'),
(16, 1, 'Tumis Kangkung', '10000', 's2.jpg', 'N'),
(17, 1, 'cumi-cumi', '50000', 'download (4).jpg', 'N'),
(18, 1, 'Sup Daging', '35000', 'images (1).jpg', 'N'),
(19, 1, 'Bihun Goreng', '15000', 'download (1).jpg', 'N'),
(20, 1, 'Salad', '15000', 's4.jpg', 'Y'),
(21, 1, 'Burger Sayur', '12000', 's2.jpg', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `id_meja` int(10) NOT NULL,
  `no_meja` int(10) NOT NULL,
  `status_meja` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id_meja`, `no_meja`, `status_meja`) VALUES
(1, 1, 'Y'),
(2, 2, 'N'),
(3, 3, 'Y'),
(4, 4, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recovery_keys`
--

INSERT INTO `recovery_keys` (`rid`, `userID`, `token`, `valid`) VALUES
(1, 3, 'ad30b7828315f12b5f2047857c1fdcec', 1),
(2, 3, '4b7ab44bc68d6829c87d7206e85030d2', 1),
(3, 3, '883c2ea5c6113601071c11d647da835a', 1),
(4, 3, '50caed4b66de223be081d0be4500f874', 1),
(5, 3, 'ac0f71b1c6ca73d12bba29ab87d8a503', 1),
(6, 3, '2a59977374f3b03a96f5c416f43033f4', 1),
(7, 3, '44c3ff39d1ceb110398cf6562ed504a5', 1),
(8, 3, 'f0a0016af591e327f965b82455b58a05', 1),
(9, 3, '20a8b9dd3d1e716aeb1eadeaa23fefb0', 1),
(10, 3, '2c51f704498142fc00c84d1ab22f8a4c', 1),
(11, 3, 'ccf95fb2711533500579c1bcf4a82b28', 1),
(12, 3, 'c549b13e2cfca8b76865f8adf5cb6b56', 1),
(13, 3, '80023d28d50e1e040aa9485387915260', 1),
(14, 3, 'b17c3b2058829ab96308c8b88de15ca4', 1),
(15, 3, 'c24acd695bdd2a03ac600ccbd0ee1ae2', 1),
(16, 4, '1b4ae2ef8fd0a734541ea9b19c37bb1d', 1),
(17, 4, 'c6f147e992f139f77b89daa7d9a1ba32', 1),
(18, 4, '2ce1b539fc89029b3e8cbe24410d993e', 1),
(19, 4, '0c22b055060a690344287f01062c8da9', 1),
(20, 4, '1a76e6a4bc1fc0b0fd2146be5f1c0188', 1),
(21, 4, '0f0673d15e06d7cd4489c32fb4fbe79e', 1),
(22, 4, '78e0be91791d5cd5bb205fcb46a95ea7', 1),
(23, 4, '4b77b2a461533f384503c5b96700eab4', 1),
(24, 4, '99c34327edad98462a937a3a91ef31b1', 1),
(25, 4, 'fd508acff89e10ac184eaedbfa49e86a', 1),
(26, 4, 'b4cc4b32fc96acb3f0f4a7e98f98f599', 1),
(27, 4, 'ec428479a4df0770ff852d57c7b6d500', 1),
(28, 4, 'fc684f1e9b77c4a0c220bdf38e2aad42', 1),
(29, 4, '86bda38004f47bf726b01336f8024822', 0),
(30, 4, '062155aaa3f37349d35bdd76013c9676', 0),
(31, 4, '9a24b317145c5f276b8f11f7afcc0bed', 0),
(32, 3, '4c30e2c7af5fec53a6d5b3ab32da4a54', 0),
(33, 3, 'eea90c477b72ec70192a612dabe7189d', 0),
(34, 3, '2191535c2b88c02a7b6c00eee6636022', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tborder`
--

CREATE TABLE `tborder` (
  `id_order` int(10) NOT NULL,
  `no_meja` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(10) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_order` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tborder`
--

INSERT INTO `tborder` (`id_order`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_order`) VALUES
(13, 1, '2019-03-31', 4, '', 'Y'),
(14, 2, '2019-03-31', 4, '', 'Y'),
(15, 2, '2019-03-31', 4, '', 'Y'),
(16, 1, '2019-03-31', 4, '', 'Y'),
(17, 2, '2019-04-01', 4, '', 'Y'),
(18, 1, '2019-04-01', 3, '', 'Y'),
(19, 2, '2019-04-01', 4, '', 'Y'),
(20, 2, '2019-04-01', 3, '', 'Y'),
(21, 3, '2019-04-01', 3, '', 'Y'),
(22, 1, '2019-04-01', 10, '', 'Y'),
(23, 2, '2019-04-01', 4, '', 'Y'),
(24, 4, '2019-04-01', 3, '', 'Y'),
(25, 4, '2019-04-01', 4, '', 'Y'),
(26, 1, '2019-04-01', 3, '', 'Y'),
(27, 2, '2019-04-01', 4, '', 'Y'),
(28, 3, '2019-04-01', 4, '', 'Y'),
(29, 4, '2019-04-01', 4, '', 'Y'),
(30, 1, '2019-04-01', 10, '', 'Y'),
(31, 2, '2019-04-01', 10, '', 'Y'),
(32, 1, '2019-04-01', 3, '', 'Y'),
(33, 2, '2019-04-01', 3, '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `id_order` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` varchar(30) NOT NULL,
  `keterangan_transaksi` varchar(30) NOT NULL,
  `uang` int(10) NOT NULL,
  `kembalian` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`, `keterangan_transaksi`, `uang`, `kembalian`) VALUES
(13, 6, 13, '2019-03-31', '14000', 'Y', 20000, 6000),
(14, 6, 14, '2019-03-31', '7000', 'Y', 10000, 3000),
(15, 6, 15, '2019-03-31', '25000', 'Y', 50000, 25000),
(16, 6, 16, '2019-03-31', '15000', 'Y', 20000, 5000),
(17, 6, 17, '2019-04-01', '25000', 'Y', 50000, 25000),
(18, 3, 18, '2019-04-01', '10000', 'Y', 10000, 0),
(19, 3, 19, '2019-04-01', '14000', 'Y', 20000, 6000),
(20, 3, 20, '2019-04-01', '21000', 'Y', 30000, 9000),
(21, 6, 21, '2019-04-01', '13000', 'Y', 15000, 2000),
(22, 6, 22, '2019-04-01', '25000', 'Y', 50000, 25000),
(23, 6, 23, '2019-04-01', '10000', 'Y', 10000, 0),
(24, 6, 24, '2019-04-01', '25000', 'Y', 30000, 5000),
(25, 6, 25, '2019-04-01', '14000', 'Y', 20000, 6000),
(26, 6, 26, '2019-04-01', '7000', 'Y', 10000, 3000),
(27, 6, 27, '2019-04-01', '25000', 'Y', 50000, 25000),
(28, 6, 28, '2019-04-01', '10000', 'Y', 10000, 0),
(29, 6, 29, '2019-04-01', '15000', 'Y', 20000, 5000),
(30, 6, 30, '2019-04-01', '15000', 'Y', 20000, 5000),
(31, 3, 31, '2019-04-01', '15000', 'Y', 20000, 5000),
(32, 3, 32, '2019-04-01', '43000', 'Y', 50000, 7000),
(33, 0, 33, '0000-00-00', '7000', 'N', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_level` int(10) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y Sudah Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `email`, `id_level`, `status`) VALUES
(3, 'widi', '21232f297a57a5a743894a0e4a801fc3', 'widiasari', 'swidiasari28@gmail.com', 1, 'Y'),
(4, 'waiter', 'f64cff138020a2060a9817272f563b3c', 'saya', 'swidiasari28@gmail.com', 2, 'Y'),
(5, 'meja4', '7f78f06d2d1262a0a222ca9834b15d9d', 'Meja 4', 'unyuunyu@gmail.com', 5, 'Y'),
(6, 'kasir', 'c7911af3adbd12a035b289556d96470a', 'Eni', 'swidiasari28@gmail.com', 3, 'Y'),
(7, 'owner', '72122ce96bfec66e2396d2e25225d70a', 'saya owner', 'swidiasari28@gmail.com', 4, 'Y'),
(8, 'meja2', '7f78f06d2d1262a0a222ca9834b15d9d', 'Meja 2', 'swidiasari28@gmail.com', 5, 'Y'),
(9, 'meja3', '7f78f06d2d1262a0a222ca9834b15d9d', 'Meja 3', 'sitiwidiasari190@gmail.com', 5, 'Y'),
(10, 'meja1', '7f78f06d2d1262a0a222ca9834b15d9d', 'Meja 1', 'swidiasari28@gmail.com', 5, 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id_detail_order`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_masakan` (`id_masakan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
  ADD PRIMARY KEY (`id_masakan`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `tborder`
--
ALTER TABLE `tborder`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id_detail_order` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
  MODIFY `id_masakan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id_meja` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tborder`
--
ALTER TABLE `tborder`
  MODIFY `id_order` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
