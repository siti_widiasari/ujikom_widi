<?php

include ('koneksi.php');
include ('function.php');

$email = $_GET['email'];
$token = $_GET['token'];

$userID = UserID($email); 

$verifytoken = verifytoken($userID, $token);


if(isset($_POST['submit']))
{
	$new_password = $_POST['password'];
	$new_password = md5($new_password);
	$retype_password = $_POST['password'];
	$retype_password = md5($retype_password);
	
	if($new_password == $retype_password)
	{
		$update_password = mysqli_query($conn, "UPDATE user SET password = '$new_password' WHERE id_user = $userID");
		if($update_password)
		{
				mysqli_query($conn, "UPDATE recovery_keys SET valid = 0 WHERE userID = $userID AND token ='$token'");
				$msg = 'Your password has changed successfully. Please login with your new password.';
				$msgclass = 'bg-success';
		}
	}else
	{
		 $msg = "Password doesn't match";
		 $msgclass = 'bg-danger';
	}
	
}


?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>DeU Resto</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="admin/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="admin/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="admin/index2.html"><b>DeU Resto</b></a>
      </div><!-- /.login-logo -->
      <?php if($verifytoken == 1) { ?>
      <div class="login-box-body">
        <p class="login-box-msg">Ganti password anda</p>
        <?php 
  if(isset($_GET['pesan'])){
    if($_GET['pesan']=="Failed to login"){
      echo "<div class='alert'>Username dan Password tidak sesuai !</div>";
    }
  }
?>
        <form action="" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="Password Baru"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Masukan Kembali Password Baru"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button  type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Ganti</button>
            </div><!-- /.col -->
          </div>
        </form>
<br>

      </div><!-- /.login-box-body -->
      			<?php }else {?>
	    	<div class="col-lg-4 col-lg-offset-4">
   		       	<h2>Invalid or Broken Token</h2>
	            <p>Opps! The link you have come with is maybe broken or already used. Please make sure that you copied the link correctly or request another token from <a href="index.php">here</a>.</p>
			</div>
        <?php }?>
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.3 -->
    <script src="admin/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="admin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>