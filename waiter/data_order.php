<?php
include "header_waiter.php";
include '../admin/database.php';
$db = new database();
?>
<link href="../admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="table-responsive">
          <div class="box-header">
            <h3 class="box-title">Data Orderan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>No Meja</th>
                <th>Tanggal</th>
                <th>Nama User</th>
                <th>Keterangan</th>
                <th>Status Order</th>
                <th>Keterangan Pembayaran</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              error_reporting(0);
              $no = 1;
              foreach($db->tampil_data_order() as $x){
                ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['no_meja']; ?></td>
                  <td><?php echo $x['tanggal']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['keterangan']; ?></td>
                  <td><?php
                                            if($x['status_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah di Konfirmasi";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum di Konfirmasi";?>
                                            <?php 
                                            }
                                            ?></td>                  
                    <td>
                    <?php
                  if($x['keterangan_transaksi'] == 'Y')
                  {
                    ?>

                    <?php echo "Sudah Dibayar";?>

                    <?php
                  }else{
                    ?>
                    <?php echo "Belum Dibayar";?>
                    <?php 
                  }
                  ?></td>
                  <td>
                                            <?php if ($x['status_order'] == 'N'){ ?>
                                              <a href="keterangan_update.php?id_order=<?php echo $x['id_order'];?>" class="btn btn-success btn-md">Konfirmasi</a> <?php }?>
                                              <a href="detail_order_waiter.php?id_order=<?php echo $x['id_order'];?>" class="btn btn-primary btn-md">Detail</a>     
                                           </td>
                <div class="modal fade" id="myModal<?php echo $x['id_transaksi'];?>" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                 <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"
                      aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"> TOTAL :
                      Rp. <?php echo number_format($x['total_bayar'], 0, ',', '.'); ?></h4>
                    </div>
                    <div class="modal-body row">
                      <div class="col-md-12">
                        <!--<form method="POST" action="?hal=cetak">-->
                          <form method="POST" action="update_transaksi.php?id_transaksi=<?php echo $x['id_transaksi'];?>">
                            <div class="form-group">
                              <label> Cash</label>
                              <input type="hidden" class="form-control" value="<?php echo $x['no_meja']; ?>" name="id_meja"/>
                              <div class="form-group">
                                <input type="text" class="form-control" id="type1" name="jumlah_uang"/>
                              </div>
                              <div class="pull-right">
                                <button class="btn btn-primary btn-sm"
                                type="submit"><i
                                class="fa fa-check-square-o"></i> OK
                              </button>
                              <button class="btn btn-danger btn-sm"
                              data-dismiss="modal" aria-hidden="true"
                              type="button"><i class="fa fa-times"></i>
                              Cancel
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </tr>
<?php 
}
?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
  </div>
  </div><!-- /.box -->
</div><!-- /.col -->
</div><!-- /.row -->
<!-- Main row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php
include "footer_waiter.php";
?>