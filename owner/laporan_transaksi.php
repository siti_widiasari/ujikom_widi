<?php
include "header_owner.php";
include '../admin/database.php';
$db = new database();
?>
<link href="../admin/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12"><div class="box">
        <div class="table-responsive">
          <div class="box-header">
            <h3 class="box-title">Data Orderan Sudah Dibayar</h3>
          </div><!-- /.box-header -->
          <form action="laporan_transaksi.php" method="post" name="postform">
            <p align="center"><font color="orange" size="3"><b>Pencarian Data Berdasarkan Periode Tanggal</b></font></p><br />
            <table border="0">
              <tr>
                <td width="125"><b>Dari Tanggal</b></td>
                <td colspan="2" width="190">: <input type="date" name="tanggal_awal" size="16" />                
                </td>
                <td width="125"><b>Sampai Tanggal</b></td>
                <td colspan="2" width="190">: <input type="date" name="tanggal_akhir" size="16" />                
                </td>
                <td colspan="2" width="190"><input type="submit" value="Pencarian Data" name="pencarian"/></td>
                <td colspan="2" width="70"><input type="reset" value="Reset" /></td>
              </tr>
            </table>
          </form>
          <div class="box-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
             <?php
             $no = 1;
                                    //proses jika sudah klik tombol pencarian data
             if(isset($_POST['pencarian'])){
                                    //menangkap nilai form
              $tanggal_awal=$_POST['tanggal_awal'];
              $tanggal_akhir=$_POST['tanggal_akhir'];
              if(empty($tanggal_awal) || empty($tanggal_akhir)){
                                    //jika data tanggal kosong
                ?>
                <script language="JavaScript">
                  alert('Tanggal Awal dan Tanggal Akhir Harap di Isi!');
                  document.location='laporan_transaksi.php';
                </script>
                <?php
              }else{
                ?><i><b>Informasi : </b> Hasil pencarian data berdasarkan periode Tanggal <b><?php echo $_POST['tanggal_awal']?></b> s/d <b><?php echo $_POST['tanggal_akhir']?></b></i>
                <?php
                $query=mysqli_query($conn,"SELECT * FROM transaksi INNER JOIN user ON transaksi.id_user = user.id_user where tanggal between '$tanggal_awal' and '$tanggal_akhir' order by tanggal ASC");
              }
              ?>
              <tr>
                <th>No</th>
                <th>Id Transaksi</th>
                <th>Nama User</th>
                <th>ID Order</th>
                <th>Tanggal</th>
                <th>Total Harga</th>
                <th>Jumlah Uang</th>
                <th>Kembalian</th>
              </tr>
            </thead> 

            <tbody>
              <?php
              $total = 0;
                                        //menampilkan pencarian data
              while($x=mysqli_fetch_array($query)){
                ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['id_transaksi']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['id_order']; ?></td>
                  <td><?php echo $x['tanggal']; ?></td>
                  <td>Rp. <?php echo number_format($x['total_bayar']); ?></td>
                  <td>Rp. <?php echo number_format($x['uang']); ?></td>
                  <td>Rp. <?php echo number_format($x['kembalian']); ?></td>
                </tr>
                <?php 
                $total = $total + $x['total_bayar'];
              } ?>
            </tbody>
            <tr>
              <td colspan="5"><strong><h4 style="margin-left: 550px;">Total</h4></strong></td>
              <td colspan="5"><strong><h4>Rp. <?php echo number_format($total);?></h4></strong></td>
            </tr>

          </table>
          <a href="lap_transaksi.php?tanggal_awal=<?php echo $_POST['tanggal_awal']?>&tanggal_akhir=<?php echo $_POST['tanggal_akhir']?>" class="btn btn-success" target="blank">Cetak</a>
          <?php
        }
        else{
          unset($_POST['pencarian']);
        }
        ?>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
</div><!-- /.col -->
</div><!-- /.row -->
<!-- Main row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php
include "footer_owner.php";
?>