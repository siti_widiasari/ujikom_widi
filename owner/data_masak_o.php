 <?php
 include 'header_owner.php';
 include'../admin/database.php';
$db = new database();
 ?>
 <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">             
              <div class="box">
                <div class="table-responsive">
                <div class="box-header">
                  <h3 class="box-title">Data Menu Makanan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="agile3-grids">
            <p align="left"><a href="#tambahmasakan" data-toggle="modal" class="btn btn-success">Cetak PDF</a></p>
          </div>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Masakan</th>
                        <th>Harga</th>
                        <th>Kategori</th>
                        <th>Gambar</th>
                        <th>Status Masakan</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
$no = 1;
foreach($db->tampil_data_masakan() as $x){
?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $x['nama_masakan']; ?></td>
                        <td><?php echo $x['harga']; ?></td>
                        <td><?php echo $x['nama_kategori']; ?></td>
                        <td><img src="../images/<?php echo $x['gambar']; ?>" height='100'></td>
                        <td><?php echo $x['status_masakan']; ?></td>
                      </tr>
<?php
include '../koneksi.php';
$id = $x['id_masakan']; 
$query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
$r = mysqli_fetch_array($query_edit);
?>
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                      
<?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php
include 'footer_owner.php';
?>