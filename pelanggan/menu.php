<?php
 include 'header_user.php';
 ?>
	<!-- special -->
	<div class="special">
		<div class="container">
			<div class="agileinfo-heading">
				<h3>Daftar Menu</h3>
			</div>
			<div class="special-top-grids">
				<div class="w3-agile-special">
<?php
include '../koneksi.php';
$query_data_masakan= mysqli_query($conn,"SELECT * FROM masakan where status_masakan='Y'");
while($data = mysqli_fetch_array($query_data_masakan)){
$harga= $data['harga'];
$hasil="Rp ".number_format($harga,2,',','.');
?>
					<div class="col-md-3 special-grids" style="margin-bottom: 15px;">
						<div class="special-img">
							<img class="img-responsive" src="../images/<?php echo $data['gambar'];?>" alt="" />
							<div class="captn">
								<div class="captn-top">
									<p><?php echo $data['nama_masakan'];?></p>
									<p><?php echo $hasil;?></p>
								</div>
								<div class="wthree-special-info">
									<h4><a href="cart.php?act=add&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=menu.php">Tambah</a></h4>
								</div>
							</div>
						</div>
					</div>
<?php
}
?>
					<!-- <div class="clearfix"> </div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- //special -->
<?php
include 'footer_user.php';
?>