<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?pesan=failed");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../admin/index.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>DeU Resto</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Food Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<!--// css -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
</head>
<body>
	<!-- banner -->
	<div class="banner jarallax">
		<!-- agileinfo-dot -->
		<div class="agileinfo-dot">
			<div class="head">
				<div class="head-nav-grids">
					<div class="navbar-top">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
								 <div class="navbar-brand logo ">
									<h1><a href="index.php">DeU <span>RESTO</span></a></h1>
								</div>

							</div>

						</div>
						<div class="header-left">
						<ul>
							<li><a href="index.php" data-hover="About">Home </a> </li>
							<li><a href="menu.php" data-hover="About">Menu </a> </li>
							<li><a class="navbar-brand" href="tabel_pesanan.php">
                                            <img src="logo.png" width="35"> </a>
                                              <span class="badge">
                <?php
                if(isset($_SESSION['items'])){
                  echo count($_SESSION['items']);
                }
                else{
                  echo "0";
                }
                ?>
        </span>
    </li>
							<li><a href="../logout.php" data-hover="About">Logout </a> </li>
						</ul>
					</div>
					<div class="clearfix"></div>	
				</div>
			</div>
		</div>
		<!-- //agileinfo-dot -->
	</div>
	<!-- //banner -->